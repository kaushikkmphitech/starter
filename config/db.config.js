import mongoose from 'mongoose';
import config from './app.config.js';
import { errorResponse } from '../utils/responseHandler.js';

export default (req, res, next) => {
    mongoose.connect(config.db.mongoURL)
        .then(() => {
            console.log('Database Connected!');
            next();
        })
        .catch(async error => {
            console.log('MONGODB ERROR: ', error.message);
            return await errorResponse(res, 500, 0, error.message);
        });
}